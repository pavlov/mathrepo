==============================================================
Sampling from the Blow up Measure 
==============================================================

This page contains the code for the experiments in Table 3 for the Blow up measure 

:download:`BlowUpMeasure.m<BlowUpMeasure.m>`


.. code-block:: none

			// We list all the partitions of 6 and the corresponding lines-counts.

			partitionCounts := [[[6], 					[0]	 ],
								[[5,1], 				[2]	 ],
								[[4, 2 ], 				[1]	 ],
								[[4, 1, 1], 			[5]	 ],
								[[3, 3], 				[0]	 ],
								[[3, 2, 1], 			[3]	 ],
								[[3, 1, 1, 1], 			[9]	 ],
								[[2, 2, 2], 			[3]	 ],
								[[2, 2, 1, 1], 			[7]	 ],
								[[2, 1, 1, 1, 1], 		[15] ],
								[[1, 1, 1, 1, 1, 1], 	[27]]];


			// The function randomPolynomial(p,N) which gives a random polynomial of degree 6.

				function randomPolynomial(p,N)

					K := pAdicField(p,300);
					_<x> := PolynomialRing(K);
				
					listCoef := [0..p^(N+1)-1];	

			    		randomPoly:= 0*x;

					for i in  [0..6] do
						coef := Random(listCoef);
						randomPoly :=  randomPoly +  coef * x^i;
					end for;

					return randomPoly;

				end function;


			// The function howManyLines(g) which computes the number of lines on the cubic surface defined by the roots of a given polynomial of degree 6.

				function howManyLines(g)

						c := -1;

						try  F := Factorization(g);

						part := Reverse( Sort([Degree(factor[1]) : factor in F ]) );

						for j in partitionCounts do
							if j[1] eq part then
								c := j[2][1];
							end if;
						end for;

						catch e c := -1;
						end try;

						return c;

				end function;

			//We determine the distribution of the lines-counts using lineCounts(p,N,M) for M polynomials.

				function lineCounts(p,N,M)

					counts := [0 : j in [-1..27]];

					c := -1;

					for i in [1..M] do

						if (i mod 1000) eq 0 then 
							print i; 
						end if;

						g := randomPolynomial(p,N);
						c := howManyLines(g);

						counts[c+2] := counts[c+2] + 1;

						while c eq -1 do
							g := randomPolynomial(p,N);
							c := howManyLines(g);
							counts[c+2] := counts[c+2] + 1;
						end while;

					end for;

					return counts;

				end function;



