==============================================================
Sampling from Tropical Measure
==============================================================

This page contains the code for the experiments in Table 3 for the Tropical measure:

:download:`TropicalMeasure.m<TropicalMeasure.m>`

.. code-block:: none

				\\we define a list called powers which gives the monomials of degree 3 in 4 variables.

					powers := [[0, 0, 0, 3],
						   [0, 0, 1, 2],
						   [0, 0, 2, 1],
						   [0, 0, 3, 0],
					       	   [0, 1, 0, 2],
					       	   [0, 1, 1, 1],
						   [0, 1, 2, 0],
					      	   [0, 2, 0, 1],
					    	   [0, 2, 1, 0],
					    	   [0, 3, 0, 0],
					    	   [1, 0, 0, 2],
						   [1, 0, 1, 1],
						   [1, 0, 2, 0],
						   [1, 1, 0, 1],
						   [1, 1, 1, 0],
						   [1, 2, 0, 0],
						   [2, 0, 0, 1],
						   [2, 0, 1, 0],
						   [2, 1, 0, 0],
						   [3, 0, 0, 0]];

				// We define the function randomSmoothCubicSurface(N,p,possiblePart) where p is a prime number and N is an integer such that we sample the coefficients from the set `{1, p, ... ,  p^N}`.

					function randomSmoothCubicSurface(N,p)
					
						_<x,y,z,w> := PolynomialRing(Rationals(),4);
						P3<x,y,z,w> := ProjectiveSpace(Rationals(),3);
					
						listCoef := [];
						for i in [0..N] do
							Append(~listCoef,p^i);
						end for;	
					
						bool := true; 
						while bool do
					
							randSurface := 0*x;		
							for part in powers do
								coef := Random(listCoef);
								monomial := x^part[1] * y^part[2] * z^part[3] * w^part[4];
								randSurface := randSurface + coef * monomial;
							end for;

							S := Surface(P3,randSurface);	
							bool := IsSingular(S);

						end while;

						return randSurface;

					end function; 


				// We use the function howManyLines(surface,p) to compute the number of lines on a fixed cubic surface using Groebner basis techniques.

					function howManyLines(surface,p)

						f := surface;
						
						QQabcd<a,b,c,d> := PolynomialRing(Rationals(),4);
						_<s,t> := PolynomialRing(QQabcd,2);

						params := [[a*s+b*t,c*s+d*t,s,t],
							   [a*s+b*t,s,c*t,t],
							   [a*s+b*t,s,t,0],
							   [s,a*t,b*t,t],
							   [s,a*t,t,0],
							   [s,t,0,0]];

						pt0 := [1,0];
						pt1 := [1,1];
						ptmin1 := [1,-1]; 
						ptinf := [0,1];

						polys := [];

						for par in params do
						
							g0 := Evaluate(f,[Evaluate(par[i],pt0) : i in [1..4]]);
							g1 := Evaluate(f,[Evaluate(par[i],pt1) : i in [1..4]]);
							gmin1 := Evaluate(f,[Evaluate(par[i],ptmin1) : i in [1..4]]);
							ginf := Evaluate(f,[Evaluate(par[i],ptinf) : i in [1..4]]);
					  
							I := Ideal([g0,g1,gmin1,ginf]);
							GB := GroebnerBasis(I);
							g := GB[#GB];
							Append(~polys,g);

						end for;

						K := pAdicField(p,750);

						nsol := 0;

						for g in polys do

							newcoeff := [];
							for i in [0..27] do
								Append(~newcoeff,K!Rationals()!Coefficient(g,4,i));
							end for;

							_<x> := PolynomialRing(K);
							gK := 0*x;
							for i in [1..28] do
								gK := gK + newcoeff[i]*x^(i-1);
							end for;

							try nsol := nsol + #Roots(gK);
							catch e return -1;
							end try;

						end for;

						return nsol;

					end function;

				// We use lineCounts(N,p,M) to determine the distribution of the line-counts for M cubic surfaces.

					function lineCounts(N,p,M)

						counts := [0 : j in [-1..27]]; 
					
						for i in [1..M] do
							print i;
						
							f := randomSmoothCubicSurface(N,p);
							count := howManyLines(f,p);
							counts[count+2] := counts[count+2] + 1; 
						end for;

						return counts;

					end function;

