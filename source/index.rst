..  image:: Banner_mathrepo1.png

.. raw:: html

  <style>
    h1 {
      display: none;
    }
  </style>



##########################
Mathematical Research Data
##########################

This is a repository of the `Max Planck Institute for Mathematics in the Sciences <https://www.mis.mpg.de>`_ in Leipzig, dedicated to mathematical research data. `Research data <https://www.forschungsdaten.info/themen/informieren-und-planen/was-sind-forschungsdaten/>`_ are all digital objects that arise during the process of doing research or are a result thereof. In particular, the purpose of this repository is to collect scripts and code, to explain applications of mathematical software, to showcase additional examples to paper publications, and more generally to host supplementary material developed for research projects or discussed in workshops.

This website is currently maintained by Tabea Bacher, Claudia Fevola and Ben Hollering. It was set up and curated by Carlos Améndola, Christiane Görgen, Lukas Kühne and Verena Morys, and Yue Ren and Mahsa Sayyary Namin from 2017 until 2022. You can contact us at `mathrepo@mis.mpg.de <mathrepo@mis.mpg.de>`_.

MathRepo is in the process of transformation. The current standards and requirements for contribution are outlined in the `Terms of Use <termsofuse.rst>`_ page. We aim to restructure the repository so that its content meets the `FAIR Principles <https://www.nature.com/articles/sdata201618>`_ for sustainable research. In the future, MathRepo will follow the guidelines developed by the Mathematical Research Data Initiative `MaRDI <https://www.mardi4nfdi.de/>`_.


.. toctree::
   :maxdepth: 1
   :hidden:

   2022.rst
   2021.rst
   2020.rst
   2019.rst
   2018.rst
   2017.rst
   GAP.rst
   Julia.rst
   M2.rst
   Magma.rst
   Maple.rst
   Mathematica.rst
   Matlab.rst
   Oscar.rst
   Polymake.rst
   Sage.rst
   Singular.rst
   events.rst
   AZ.rst
   termsofuse.rst