========================
Code and implementation
========================

The file :download:`main_algorithm.m2 <main_algorithm.m2>` implements Algorithm 2 in our paper. The code also uses the file :download:`eqnTangential.m2 <eqnTangential.m2>` that implements equations for the tangential variety of a Segre variety :math:`X_{1^k}`, for some :math:`k\geq 3`.

**Input:** a concise tensor :math:`T=(t_{i_1,\dots, i_{k}})\in \mathcal{T}_{n_1,n_2,2^{k-2}}`,for some :math:`k>3`, :math:`2\leq n_1,n_2\leq 3`. 

**Output:** a statement telling if the given tensor is a rank-3 tensor that falls into one of the cases mentioned above or not. 

Below a pseudocode description of the algorithm.

**0.** For all :math:`i,j=1,\dots,k` with :math:`i\neq j` denote by :math:`\theta_{i,j}` the reshape grouping the :math:`i`-th and :math:`j`-th factor of :math:`\mathcal{T}_{n_1,\dots,n_{k}}`.

**1.** Case :math:`(n_1,n_2)=(2,2)`.

   - Case :math:`k=4`. Test if :math:`T\in \sigma_3(X_{1^4})\setminus \sigma_2(X_{1^4})`. If the answer to the test is positive, the output is: :math:`T` *is a non-identifiable rank-3 tensor*, otherwise the output is:  :math:`T` *is not on the list of non-identifiable rank-3 tensors*.
   
   - Case :math:`k\geq 5`. For all :math:`i=1,\dots,k-1` and for all :math:`j=i+1\dots,k` follow this procedure:

      - Test if :math:`\theta_{i,j}(T)` satisfies the equations of :math:`\sigma_2(X_{3,1^{k-2}})` and does not satisfy the equations of :math:`\tau(X_{3,^{k-2}})`. If :math:`\theta_{i,j}(T)\in \sigma_2(X_{3,1^{k-2}})\setminus \tau(X_{3,1^{k-2}})` then :math:`\theta_{i,j}(T)` is an identifiable rank-2 tensor. 
        Make the concision process on the first factor of :math:`\mathcal{T}_{3,1^{k-2}}` and call :math:`T'` the resulting tensor. 
        Consider :math:`T'` as a matrix pencil of :math:`\mathbb{C}^2\otimes \mathbb{C}^2\otimes( (\mathbb{C}^2)^{\otimes(k-2)})` with respect to the second factor

         .. math::
            T'=\lambda C_1 + \mu C_2.

         
       Find the eigenvectors :math:`x,y\in \mathbb{C}^2` of :math:`C_1C_2^{-1}` and then rewrite :math:`x,y` as elements of :math:`\mathbb{C}^{4}\cong \mathbb{C}^2\otimes \mathbb{C}^2` via :math:`\theta^{-1}_{i,j}`. If :math:`\{r(x),r(y) \}=\{ 1,2\}` then the output is: :math:`T` *is a non-identifiable rank-3 tensor corresponding to case 6 of the identifiability Theorem*.

      - Else, if one of the previous conditions is not satisfied, then stop and restart with another :math:`j` (and another :math:`i` when necessary).
   
      If the algorithm stops at some point when :math:`i=k-1,j=k` then break and the output is: :math:`T` *is not on the list of non-identifiable rank-3 tensors*.


2. Case :math:`(n_1,n_2)=(3,2)`.
For all :math:`i=2,\dots,k-1` follow this procedure:
   - Test if :math:`\theta_{1,i}(T)` satisfies the equations of :math:`\sigma_2(X_{5,1^{k-2}})` and does not satisfy the equations of :math:`\tau(X_{5,^{k-2}})`. If :math:`\theta_{1,i}(T)\in \sigma_2(X_{5,1^{k-2}})\setminus \tau(X_{5,1^{k-2}})` then :math:`\theta_{1,i}(T)` is an identifiable rank-2 tensor. Reduce the first factor of :math:`\mathcal{T}_{6,2^{k-2}}` via concision, working now on :math:`\mathcal{T}_{2^{k-1}}` with :math:`T'`. Consider :math:`T'` as a matrix pencil with respect to the second factor of :math:`\mathbb{C}^2\otimes \mathbb{C}^2\otimes (\mathbb{C}^2)^{\otimes (k-3)}`, i.e. 

      .. math::
         T'=\lambda C_1 + \mu C_2.

    Find the eigenvectors :math:`x,y` of :math:`C_1C_2^{-1}` and then rewrite :math:`x,y` as elements of :math:`\mathbb{C}^6=\mathbb{C}^3\otimes \mathbb{C}^2` via :math:`\theta^{-1}_{1,i}`. If :math:`\{ r(x),r(y)\}=\{ 2,1 \}` the output is: :math:`T` *is a non-identifiable rank-3 tensor*.

   - Else, if one of the previous conditions is not satisfied then stop and restart with another :math:`i`.
   
   If the algorithm stops at some point when :math:`i=k` then break and the output is: :math:`T` *is not on the list of non-identifiable rank-3 tensors*.

3. Case :math:`(n_1,n_2)=(3,3)`.
   
   - Test if :math:`\theta_{1,2}(T)` satisfies the equations of :math:`\sigma_2(X_{8,1^{k-2}})` and does not satisfy the equations of :math:`\tau(X_{8,^{k-2}})`. If :math:`\theta_{1,2}(T)\in \sigma_2(X_{8,1^{k-2}})\setminus \tau(X_{8,1^{k-2}})` then :math:`\theta_{1,2}(T)` is an identifiable rank-2 tensor. Reduce the first factor of :math:`\mathcal{T}_{9,2^{k-2}}` via the concision process, working now with :math:`T'` on :math:`(\mathbb{C}^2)^{\otimes (k-1)}`. Consider :math:`T'` as a matrix pencil with respect to the second factor of :math:`\mathbb{C}^2\otimes \mathbb{C}^2\otimes (\mathbb{C}^2)^{\otimes (k-3)}`, i.e. 

      .. math::
         T'=\lambda C_1+\mu C_2.
   
    Find the eigenvectors :math:`x,y` of :math:`C_1C_2^{-1}` and then rewrite :math:`x,y` as elements of :math:`\mathbb{C}^9\cong \mathbb{C}^3\otimes \mathbb{C}^3` via :math:`\theta^{-1}_{1,2}`. If :math:`\{ r(x),r(y) \}=\{1,2 \}` the output is: :math:`T` *is a non-idenfitiable rank-3 tensor as in case 6*.

   - If one of these conditions is not satisfied then stop and the output is: :math:`T` *is not on the list of non-identifiable rank-3 tensors*.
