========================
Code and implementation
========================

The file :download:`makingWaves.m2 <makingWaves.m2>` implements Algorithm 1 in our paper via the command ``wavePairs(A, r)``.
The input ``A`` is an :math:`\ell \times k` matrix in a polynomial ring in :math:`n` variables over :math:`\mathbb{Q}`, and ``r`` is an integer between :math:`0` and :math:`n-1`.
The output is the saturated ideal of the wave pair variety :math:`\mathcal{P}_r^A`.
This ideal lies in the coordinate ring of :math:`\operatorname{Gr}(n-r, n) \times \mathbb{P}^{k-1}`. This is represented in Macaulay2 by the ring :math:`\mathbb{Q}[p_I, z_1,\dotsc, z_k]/G`, where the :math:`p_I` represent Plücker coordinates, (:math:`I` ranges over all subsets of :math:`\{1,2,\dotsc,n\}` of size :math:`n-r`) and :math:`G` is the ideal of Plücker relations.

By default, the computation is performed globally, and thus can be rather slow. This can be sped up by performing the computation locally on an affine patch of the Grassmannian, which can be achieved by specifying the optional argument ``Patch => I``. Valid types for ``I`` are ``List``, ``ZZ``, or ``Boolean``. If :math:`I` is list representing a subset of :math:`\{1,2,\dotsc,n\}` of size :math:`n-r`, setting ``Patch => I`` will restrict to the affine patch where the coordinate :math:`p_I = 1`. Alternatively, :math:`I` can be an integer between :math:`0` and :math:`\binom{n}{n-r} - 1`. The option ``Patch => true`` is equivalent to ``Patch => 0`` and ``Patch => toList(0..<n-r)``.

For convenience, we also provide the option ``ModPlucker => false``, which returns the lift of the wave pair ideal in :math:`\mathbb{Q}[p_I, z_1,\dotsc,z_k]`.

For an example showcasing options and usage, see the page :doc:`example`.

Source code
----------------------------

.. literalinclude:: makingWaves.m2
   :language: macaulay2
   :linenos:
