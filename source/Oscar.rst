####################
 OSCAR
####################

- :doc:`BranchPoints/index`

- :doc:`CountingChambers/index`

- :doc:`FiberZonotopes/index`

- :doc:`intersection-bodies/index`

- :doc:`ToricGeometry/index`

- :doc:`OSCAR/index`

- :doc:`OrdersPolytropes/index`


.. toctree::
   :maxdepth: 0
