using Oscar
a = Perm([2,1,3,4])
b = Perm([3,2,1,4])
c = Perm([4,2,3,1])
d = Perm([1,3,2,4])
e = Perm([1,4,3,2])
f = Perm([1,2,4,3])
id = Perm([1,2,3,4])


# Computes whether a given monodromy representation is real, using the algorithm in Cadoret "Counting real Galois covers of the projective line", 
# Section 3.3
# Input: pairs is the number of pairs of complex conjugate points, real is the number of real branch pts, monod is the monodromy representation, in is the involution
function is_real_monodromy_representation(monod::Vector{Perm{Int64}},in::Perm, real::Int, pairs::Int)
    m = monod[1]
    for i in 1:real
        tau = in*m*in
        if tau != inv(m)
            return false
        end
        if i != real
            m = m*monod[i+1]
        end
    end
    for j in 1:pairs
        if in*monod[2j+real-1]*in != monod[2j+real]
            return false
        end
    end 
    return true
end

# Input: a list of monodromy representations, an involution in, pairs is the number of pairs of complex conjugate points, real is the number of real branch pts.
# Output: a list of Integers, returning the indexes in the array monod_list that are real monodromy representations
function compute_real_monodromy_list(monodlist::Vector{Vector{Perm{Int64}}},in::Perm, real::Int, pairs::Int)
    V =  Vector{Int64}([])
        for i in 1:length(monodlist)
            f = is_real_monodromy_representation(monodlist[i],in,real,pairs)  
            if f==true
                push!(V,i)
            end
        end
    return V
end


