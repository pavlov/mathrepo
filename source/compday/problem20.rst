==========
Problem 20
==========

.. compday_problem20:

Fix the Puiseux series field :math:`R = \mathbb{R}\{\!\{ \epsilon \}\!\}`. Pick a matrix :math:`A \in
\mathbb{R}^{4 \times 8}`, column vector :math:`b \in R^4` and row vector :math:`c \in R^8`, with entries that
look random, with powers of :math:`\epsilon`. Solve the linear programming problem

  Maximize :math:`c \cdot x` subject to :math:`A x = b` and :math:`x \geq 0`

Solution
========

In *Polymake*:

.. code-block:: perl

                application "polytope";
                $x = new PuiseuxFraction<Min>(new UniPolynomial<Rational,Rational>('x'));
                $A = new Matrix<PuiseuxFraction<Min>>([[2,3,$x*$x,11]]);
                $c = new Vector<PuiseuxFraction<Min>>([0,1,2+$x]);
                $P = new Polytope<PuiseuxFraction<Min>>(INEQUALITIES=>zero_vector|unit_matrix($A->cols-1),EQUATIONS=>$A);
                print $P->LP(LINEAR_OBJECTIVE=>$c)->MAXIMAL_VALUE;

.. image:: 20.png
