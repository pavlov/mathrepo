#########################################
Geometry of the Main Component (improved)
#########################################

This page contains the improved code we used to prove that for :math:`g\leq 9` the main component of the Hirota variety :math:`\mathcal{H}_{\mathcal{C}}^M` is an irreducible component of dimension :math:`3g`. This was achieved by computing the ideal defining the main component :math:`\mathcal{H}_{\mathcal{C}}^M` using the explicit equations described in Section 3. Lemma 7 and Theorem 10 avoid the computation of redundant generators.    
In particular, here we show the code for :math:`g=6`. If you want to run the code yourself, you may download the following file :download:`main_component_fast.m2 <main_component_fast.m2>`.

To generate the entries of the cube :math:`\mathcal{C}=\{0,1\}^g` that index the variables :math:`{\bf a}=(a_1,a_2,\dots,a_{2^g})` we used the function in :math:`\texttt{Mathematica}`:

.. code-block:: mathematica
		
		g = 6;
	   	{Array[List, ConstantArray[2, g], 0, ## &]}
	   	

This is the only part of the code depending on the genus, the rest of is generalised to any genus :math:`g`. This is implemented in :math:`\texttt{Macaulay2}`.

.. code-block:: macaulay2

                restart
		C = ZZ/32999

		g = 6
		r = 2^g+3*g --number parameters in product space  
		t = 2*g+g+1 --number of kappas and lambdas
		p = 2^g-1     --number of a's
		cube = {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 1, 0}, {0, 0, 0,
		   0, 1, 1}, {0, 0, 0, 1, 0, 0}, {0, 0, 0, 1, 0, 1}, {0, 0, 0, 1, 1, 
		  0}, {0, 0, 0, 1, 1, 1}, {0, 0, 1, 0, 0, 0}, {0, 0, 1, 0, 0, 1}, {0, 
		  0, 1, 0, 1, 0}, {0, 0, 1, 0, 1, 1}, {0, 0, 1, 1, 0, 0}, {0, 0, 1, 1,
		   0, 1}, {0, 0, 1, 1, 1, 0}, {0, 0, 1, 1, 1, 1}, {0, 1, 0, 0, 0, 
		  0}, {0, 1, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0}, {0, 1, 0, 0, 1, 1}, {0, 
		  1, 0, 1, 0, 0}, {0, 1, 0, 1, 0, 1}, {0, 1, 0, 1, 1, 0}, {0, 1, 0, 1,
		   1, 1}, {0, 1, 1, 0, 0, 0}, {0, 1, 1, 0, 0, 1}, {0, 1, 1, 0, 1, 
		  0}, {0, 1, 1, 0, 1, 1}, {0, 1, 1, 1, 0, 0}, {0, 1, 1, 1, 0, 1}, {0, 
		  1, 1, 1, 1, 0}, {0, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 0, 0}, {1, 0, 0, 0,
		   0, 1}, {1, 0, 0, 0, 1, 0}, {1, 0, 0, 0, 1, 1}, {1, 0, 0, 1, 0, 
		  0}, {1, 0, 0, 1, 0, 1}, {1, 0, 0, 1, 1, 0}, {1, 0, 0, 1, 1, 1}, {1, 
		  0, 1, 0, 0, 0}, {1, 0, 1, 0, 0, 1}, {1, 0, 1, 0, 1, 0}, {1, 0, 1, 0,
		   1, 1}, {1, 0, 1, 1, 0, 0}, {1, 0, 1, 1, 0, 1}, {1, 0, 1, 1, 1, 
 		 0}, {1, 0, 1, 1, 1, 1}, {1, 1, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 1}, {1, 
 		 1, 0, 0, 1, 0}, {1, 1, 0, 0, 1, 1}, {1, 1, 0, 1, 0, 0}, {1, 1, 0, 1,
 		  0, 1}, {1, 1, 0, 1, 1, 0}, {1, 1, 0, 1, 1, 1}, {1, 1, 1, 0, 0, 
 		 0}, {1, 1, 1, 0, 0, 1}, {1, 1, 1, 0, 1, 0}, {1, 1, 1, 0, 1, 1}, {1, 
 		 1, 1, 1, 0, 0}, {1, 1, 1, 1, 0, 1}, {1, 1, 1, 1, 1, 0}, {1, 1, 1, 1,
		   1, 1}};

  
		--create the a_i generators
		agens = {};
		for i from 0 to p do 
		    agens=append(agens,a_(cube_i))
		#agens == 2^g

		--create the vectors of the degrees 
		deg = {}; 
		for i from 1 to g do
		    deg = append(deg,1)
		for i from 1 to g do
		    deg = append(deg,2)
		for i from 1 to g do
		    deg = append(deg,3)
		for i from 1 to p+1 do
		    deg = append(deg,0)
		--    
		R = C[u_1..u_g,v_1..v_g,w_1..w_g,agens,Degrees=>deg];
                #(generators R)==r
                kappas=2*g, lambdas=g
                T = C[k_1..k_kappas,l_1..l_lambdas];
                t == #(generators T)
                S = R[q_1..q_g];
                --extract a_i generators
                Agens = {};
                for i from 3*g to r-1 do
                Agens = append(Agens,(generators R)_i)
                #Agens==2^g


Now we generate the ideal defining the Hirota variety using the correspondence between the points in the set :math:`\mathcal{C}^{[2]}` (described in Section 3) and the equations cutting :math:`\mathcal{H}_{\mathcal{C}}^M`.

.. code-block:: macaulay2

                hirotaGen = {};
                --first we define the polynomials corresponding to point attained uniquely in C^{[2]}
                for i from 1 to g do 
                hirotaGen = append(hirotaGen, u_i^4-4*u_i*w_i+3*v_i^2)
                hirotaGen

                --we compute the set C^[2]
                --elements in C2mult are {{c_i+c_j}, (),(),()}
                --in the () there are all possible pairs giving c_i+c_j
                C2mult = new MutableList from {}; 

                for i from 0 to 2^g-2 do {
                    for j from i+1 to 2^g-1 do {
	                c2 = cube_i+cube_j;	
                        if i==0 then {
	                   if j==1 then { 
	                      C2mult#0 = {c2}}},	
	                      f = 0,     
	                      for k from 0 to #C2mult-1 do {
	                      if c2 == (C2mult#k)#0 then {
	                         f = 1,
	                         temp = flatten{C2mult#k,(i,j)},
	                         C2mult#k = temp
	                   }};  
	               if f == 0 then {
	                  l = #C2mult,
	                  C2mult#l = {c2,(i,j)}} 
                        } 
                }
                #C2mult


                --C2more contains all elements which are attained at least twice
	        C2more = {};

                for i from 0 to #C2mult-1 do {
                    if #(C2mult#i)>2 then 
                       C2more = append(C2more,(C2mult#i))
                }  
                #C2more


                --C2dir contains element attained at least twice and one per "direction"-class
                --this corresponds to remove all elements containing a zero entry
                C2dir = {};

                for i from 0 to #C2more-1 do {
                m = 1,
                for j from 0 to g-1 do {
	            m = m * (((C2more#i)#0)#j)
                },
                if m != 0 then 
                   C2dir = append(C2dir,C2more#i) 	    
                }  
                #C2dir


                --Now we compute the polynomials corresponding to the points in C2dir
                --in this way we will compute directly the ideal defining the main component of the Hirota variety
                --define the vectors u,v,w
                uVec = mutableMatrix(R,1,g),vVec = mutableMatrix(R,1,g),wVec = mutableMatrix(R,1,g);
                for i from 0 to 0 do{
                    for j from 0 to g-1 do {
                        uVec_(i,j) = u_(j+1),
	                vVec_(i,j) = v_(j+1),
	                wVec_(i,j) = w_(j+1)
                    }	
                 }
                uVec = matrix uVec, vVec = matrix vVec, wVec = matrix wVec;
               
                P = (x,y,t) -> x^4-4*x*t+3*y^2

                --now we compute the remaining generators of main component of the Hirota variety 
                for i from 0 to #C2dir-1 do {
                    temp = 0,
                    for j from 1 to #(C2dir#i)-1 do {
	                k = ((C2dir#i)#j)#0,
	                l = ((C2dir#i)#j)#1,
	                temp = temp + a_(cube_k)*a_(cube_l)*P(uVec*transpose(matrix{cube_k}-matrix{cube_l}),vVec*transpose(matrix{cube_k}-matrix{cube_l}),wVec*transpose(matrix{cube_k}-                        matrix{cube_l}))
                }, 
                hirotaGen = append(hirotaGen,temp)	
                } 
                #hirotaGen == g + #C2dir   

                MainComp = ideal(hirotaGen);
                use R

                J = jacobian(MainComp);
                --In what follows we define the parametrisation and then we plug it in the Jacobian of the Hirota variety. 
                --To do this we assume the parameters and the parameters to be random values in the field ,
                -- otherwise a completely symbolic computation would be computationally too hard.

                --we create the list of indices appearing in the kappas in the parametrization for each vertex of the cube
                I = mutableMatrix(ZZ,g,2^g);
                for k from 0 to p do {
                    for i from 0 to g-1 do{
                        if i == 0 then {
                           if (cube_k)_i == 0 then
                           I_(i,k) = 2
                     else
                           I_(i,k) = 1}
                     else
                           if (cube_k)_i == 0 then
                           I_(i,k) = 2*i+2
                     else
                           I_(i,k) = 2*i+1
                  }
                 }


                --we create a matrix whose entries are the parametrization for the a_i's
                param = mutableMatrix(C,1,p+1);
                --initialize the matrix param to have all 1's as entries
                for i from 0 to 0 do {
                    for j from 0 to p do
                    param_(i,j) = 1
                 }
                -- vector of random entries for the kappa parameters
                kk={};
                for i from 0 to kappas-1 do
                kk = append(kk,random(C));
                #kk == kappas
                --parametrization for the a's
                for i from 0 to 0 do {
                    for j from 0 to p do {
                        for t from 0 to g-2 do {
                            for s from t+1 to g-1 do
                                param_(i,j)=param_(i,j)*(kk_(I_(t,j)-1) - kk_(I_(s,j)-1))
                        }
                     }
                 }

                --parametrization for u,v,w
		Puvw = {};
		for i from 1 to kappas-1 do {
		      if i%2==1 then
		      Puvw = append(Puvw,kk_(i-1)-kk_(i));
		}
		for i from 1 to kappas-1 do {
		      if i%2==1 then
		      Puvw = append(Puvw,kk_(i-1)^2-kk_(i)^2);
		}
		for i from 1 to kappas-1 do {
 		     if i%2==1 then
		      Puvw = append(Puvw,kk_(i-1)^3-kk_(i)^3);
		}

		G = mutableMatrix(C,1,r);
		for i from 0 to 0 do {
 		   for j from 0 to 3*g-1 do
		        G_(i,j) = Puvw_j;
		    for j from 3*g to r-1 do
		        G_(i,j) = param_(0,j-3*g)
		        }
		G = matrix G;


		f = map(C,R,G);


		expectedRank = (r-1)-3*g
		tang = f(J);
		rank tang
		rank tang == expectedRank