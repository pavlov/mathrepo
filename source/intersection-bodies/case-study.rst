

==========================
The 3-cube and the origin
==========================

The intersection body :math:`IP` is constructed via intersections of the polytope :math:`P` with hyperplanes that go through the origin. Thus, the position of the origin relative to :math:`P` plays an important role and has drastic effects on the shape of :math:`IP`.
On this page, we present four different such cases, when :math:`P = C^{(3)}` is the 3-dimensional cube. 
The color of a piece of the boundary of :math:`IP` indicates the degree of the equation that defines this piece. The corresponding facet of the dual of the zonotope :math:`Z(P)` is colored accordingly. If the origin does not lie in the interior of :math:`P`, then there are regions :math:`C \in \mathbb R^3` such that for the radial function :math:`\rho` holds :math:`\rho(x) = 0 ` for all :math:`x \in C`. The corresponding facets of :math:`Z(P)^\circ` are then colored in black.

.. image:: legend.png
    :width: 40%
    :align: center

The origin is the center
-------------------------

When :math:`P = [-1,1]^3`, then :math:`IP` is a convex body.  The hyperplane arrangement :math:`H` consists of 14 maximal regions. Equivalently, the dual of the zonotope :math:`Z(P)` has 14 facets.

On the left, you see a 3d model of the :math:`IP`. On the right is a 3d model of the dual of the zonotope :math:`Z(P)`. 


.. precision = 200

.. raw:: html

    <iframe src="../_static/intersection-bodies-3d/cube1.html"  width="45%" height = "400px" frameBorder="0"></iframe>
    <iframe src="../_static/intersection-bodies-3d/zonotope1.html"  width="45%" height = "400px" frameBorder="0"></iframe>

Click here to see the 3d models in full screen: `[Intersection Body] <../_static/intersection-bodies-3d/cube1.html>`_   `[Dual of Zonotope] <../_static/intersection-bodies-3d/zonotope1.html>`_ 
  

The origin lies somewhere in :math:`C^{(3)}`
---------------------------------------------

Here is the intersection body of :math:`P = [-1,1]^3 + (\frac{1}{2}, \frac{1}{3}, \frac{1}{4})`. The polytope :math:`Z(P)^\circ` has 58 facets.

.. precision = 150

.. raw:: html

    <iframe src="../_static/intersection-bodies-3d/cube2.html"  width="45%" height = "400px" frameBorder="0"></iframe>
    <iframe src="../_static/intersection-bodies-3d/zonotope2.html"  width="45%" height = "400px" frameBorder="0"></iframe>

Click here to see the 3d models in full screen: `[Intersection Body] <../_static/intersection-bodies-3d/cube2.html>`_   `[Dual of Zonotope] <../_static/intersection-bodies-3d/zonotope2.html>`_ 


The origin lies in the interior of a facet of :math:`C^{(3)}`
--------------------------------------------------------------

.. precision = 200

The intersection body of :math:`P = [-1,1]^3 + (0,0,1)`. The polytope :math:`Z(P)^\circ` has 28 facets.

.. raw:: html

    <iframe src="../_static/intersection-bodies-3d/cube3.html"  width="45%" height = "400px" frameBorder="0"></iframe>
    <iframe src="../_static/intersection-bodies-3d/zonotope3.html"  width="45%" height = "400px" frameBorder="0"></iframe>

Click here to see the 3d models in full screen: `[Intersection Body] <../_static/intersection-bodies-3d/cube3.html>`_   `[Dual of Zonotope] <../_static/intersection-bodies-3d/zonotope3.html>`_ 


The origin lies on an edge
---------------------------

The intersection body of :math:`P = [-1,1]^3 + (0,1,1)`. The polytope :math:`Z(P)^\circ` has 38 facets.

.. precision = 180

.. raw:: html

    <iframe src="../_static/intersection-bodies-3d/cube4.html"  width="45%" height = "400px" frameBorder="0"></iframe>
    <iframe src="../_static/intersection-bodies-3d/zonotope4.html"  width="45%" height = "400px" frameBorder="0"></iframe>

Click here to see the 3d models in full screen: `[Intersection Body] <../_static/intersection-bodies-3d/cube4.html>`_   `[Dual of Zonotope] <../_static/intersection-bodies-3d/zonotope4.html>`_ 


The origin is a vertex of :math:`C^{(3)}`
------------------------------------------

The intersection body of :math:`P = [-1,1]^3 + (1,1,1) = [0,2]^3`. It consists of 32 regions.

.. precision = 180

.. raw:: html

    <iframe src="../_static/intersection-bodies-3d/cube5.html"  width="45%" height = "400px" frameBorder="0"></iframe>
    <iframe src="../_static/intersection-bodies-3d/zonotope5.html"  width="45%" height = "400px" frameBorder="0"></iframe>

Click here to see the 3d models in full screen: `[Intersection Body] <../_static/intersection-bodies-3d/cube5.html>`_   `[Dual of Zonotope] <../_static/intersection-bodies-3d/zonotope5.html>`_ 



The origin lies outside of :math:`C^{(3)}`
-------------------------------------------

The shape of the intersection body is still not determined if the origin lies outside the polytope. It it still relevant *where* outside the polytope the origin lies, i.e. whether the origin is a point beyond a facet, edge or vertex. Depending on the case, the shape of the intersection body will be similar to one of the above, i.e. just like when the origin lies on the respective face. In this case, on some regions the radial function will be zero. This phenomenon can already be seen in previous example, where the origin is a vertex of the cube.
