================
Sections 1 and 2
================

1.4. Computational methods
==========================

We define a function :math:`\verb|hessRank2|` that takes as input a quaternary cubic, and outputs the ideal defining the rank 2 matrices in :math:`H(f)`. If :math:`\verb|xCoords == false|`, the ideal is computed in the :math:`z_{ij}` coordinates, i.e. we view :math:`H(f)` as a subspace of the space of symmetric :math:`4 \times 4` matrices. If :math:`\verb|xCoords == true|`, the ideal is computed in the :math:`x_i` coordinates, i.e. we use the Hessian matrix to identify :math:`H(f)` with :math:`\mathbb{P}^3`.

.. code-block:: macaulay2

		K=QQ;
		R=K[x_0..x_3,l_0..l_4,z00,z01,z02,z03,z11,z12,z13,z22,z23,z33];
		X={x_0,x_1,x_2,x_3};
		A=genericSymmetricMatrix(R,z00,4);
		I2=minors(3,A);
		hessRank2 = {xCoords=>false} >> o -> f ->(
		    hess = diff(transpose matrix{X},diff(matrix{X},f));
		    I=eliminate(X,ideal(flatten entries (A-hess)));
		    J=I+I2;
		    if o.xCoords then(
		        J=sub(J,flatten apply(4,i->apply(i+1,j->(A_(i,j)=>hess_(i,j)))));
		    );
		    return J;
		);
		isOnHessianDiscriminant = f ->(
		    J=hessRank2(f);
		    return not ((codim J==9) and (J==radical J));
		);

Some examples:

.. code-block:: macaulay2
		
		f=x_0*x_1*x_2+x_0*x_1*x_3+x_0*x_2*x_3+x_1*x_2*x_3;
		isOnHessianDiscriminant(f)
		--false
		f=x_0^3+x_1^3+x_2^3+x_3^2*(3*x_0+3*x_1+3*x_2+x_3);
		isOnHessianDiscriminant(f)
		--true

2.1. Sylvester's pentahedral form
=================================

We compute :math:`H(f) \cap X_2` for cubics in Sylvester's pentahedral form.

.. code-block:: macaulay2

		x_4=-sum(4,i->(x_i));
		f=sum(5,i->(l_i*x_i^3));
		J=hessRank2(f);
		P=primaryDecomposition J;
		toString P
		
We find the following primat primary decomposition, as described in remark 2.3:

.. code-block:: macaulay2

		{ideal(z22-z23,z13-z23,z12-z23,z11-z23,z03-z23,z02-z23,z01-z23,z00-z23,l_3*z23-l_4*z23+l_4*z33),ideal(z33,z23,z22,z13,z12,z03,z02,z01,l_1*z00+l_0*z11),ideal(z33,z23,z22,z13,z12,z03,z02,z01,l_4),ideal(z23,z13,z12,z11,z03,z02,z01,z00,l_4),ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_4),ideal(z23,z22,z13,z12,z03,z02,z01,z00,l_4),ideal(z33,z23,z13,z12,z11,z03,z02,z01,l_4),ideal(z23,z22,z13,z12,z11,z03,z02,z01,l_4),ideal(z23,z13,z12,z11,z03,z02,z01,z00,l_1),ideal(z23,z13,z12,z11,z03,z02,z01,z00,l_0),ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_0),ideal(z23,z22,z13,z12,z03,z02,z01,z00,l_0),ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_3),ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_2*z11+l_1*z22),ideal(z23,z22,z13,z12,z03,z02,z01,z00,l_2),ideal(z23,z22,z13,z12,z03,z02,z01,z00,l_3*z11+l_1*z33),ideal(z23,z13,z12,z11,z03,z02,z01,z00,l_3*z22+l_2*z33),ideal(z33,z23,z13,z12,z11,z03,z02,z01,l_1),ideal(z23,z22,z13,z12,z11,z03,z02,z01,l_1),ideal(z33,z23,z13,z12,z11,z03,z02,z01,l_3),ideal(z33,z23,z13,z12,z11,z03,z02,z01,l_2*z00+l_0*z22),ideal(z23,z22,z13,z12,z11,z03,z02,z01,l_2),ideal(z23,z22,z13,z12,z11,z03,z02,z01,l_3*z00+l_0*z33),ideal(z33,z23,z22,z13,z12,z03,z02,z01,l_3),ideal(z33,z23,z22,z13,z12,z03,z02,z01,l_2),ideal(z22-z23,z13-z23,z12-z23,z11-z23,z03-z23,z02-z23,z01-z23,z00-z23,l_2),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_2),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,l_2),ideal(z22-z23,z13-z23,z12-z23,z11-z23,z03-z23,z02-z23,z01-z23,z00-z23,l_1),ideal(z23-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_1),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,l_1),ideal(z22-z23,z13-z23,z12-z23,z11-z23,z03-z23,z02-z23,z01-z23,z00-z23,l_0),ideal(z23-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_0),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_0),ideal(z23-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_3),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_3),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,l_3),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,l_4*z00+l_0*z33-l_4*z33),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_4*z11+l_1*z33-l_4*z33),ideal(z23-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_4*z22+l_2*z33-l_4*z33)}
		--length P==40
		
If we assume additionally that all l_i are nonzero (which amounts to saturating our ideal), the primary decomposition becomes much shorter:

.. code-block:: macaulay2

		Jsat =saturate(J,ideal product(5,i->l_i));
		Psat = primaryDecomposition(Jsat);
		toString Psat

.. code-block:: macaulay2
		
		{ideal(z22-z23,z13-z23,z12-z23,z11-z23,z03-z23,z02-z23,z01-z23,z00-z23,l_3*z23-l_4*z23+l_4*z33),ideal(z23,z13,z12,z11,z03,z02,z01,z00,l_3*z22+l_2*z33),ideal(z23,z22,z13,z12,z03,z02,z01,z00,l_3*z11+l_1*z33),ideal(z23,z22,z13,z12,z11,z03,z02,z01,l_3*z00+l_0*z33),ideal(z33,z23,z22,z13,z12,z03,z02,z01,l_1*z00+l_0*z11),ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_2*z11+l_1*z22),ideal(z33,z23,z13,z12,z11,z03,z02,z01,l_2*z00+l_0*z22),ideal(z23-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_4*z22+l_2*z33-l_4*z33),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z03-z33,z02-z33,z01-z33,z00-z33,l_4*z11+l_1*z33-l_4*z33),ideal(z23-z33,z22-z33,z13-z33,z12-z33,z11-z33,z03-z33,z02-z33,z01-z33,l_4*z00+l_0*z33-l_4*z33)}
		
Upon inspecting Psat, we see that for any cubic in sylvester pentahedral form with all coefficients :math:`\lambda_i` nonzero, the 10 points in :math:`H(f) \cap X_2` are distinct

In x-coordinates:

.. code-block:: macaulay2
		
		J=saturate(hessRank2(f,xCoords=>true),ideal product(5,i->l_i));
		Psat = primaryDecomposition(J);
		toString Psat

.. code-block:: macaulay2

		{ideal(x_3,x_2,x_1),ideal(x_2,x_1,x_0+x_3),ideal(x_2,x_1,x_0),ideal(x_3,x_2,x_0+x_1),ideal(x_3,x_2,x_0),ideal(x_2,x_1+x_3,x_0),ideal(x_3,x_1,x_0+x_2),ideal(x_3,x_1,x_0),ideal(x_3,x_1+x_2,x_0),ideal(x_2+x_3,x_1,x_0)}


2.2 Rank six cubics
===================

We now turn our attention to general rank 6 cubics.

.. code-block:: macaulay2

		f=x_1^3+x_2^3+x_3^3-x_0^2*(l_0*x_0+3*l_1*x_1+3*l_2*x_2+3*l_3*x_3);
		J=hessRank2(f);
		--We assume that l_1,l_2,l_3 are nonzero:
		Jsat =saturate(J,ideal(l_1*l_2*l_3));
		Psat = primaryDecomposition(Jsat);
		print toString Psat
		print toString apply(Psat,e->radical e)

.. code-block:: macaulay2

		{ideal(z23,z13,z12,z11,z03,z02,z01,z00,l_2*z22+l_3*z33), ideal(z23,z22,z13,z12,z11,z03^2,z02*z03,z01*z03,z02^2,z01*z02,l_3*z02-l_2*z03,z01^2,l_3*z01-l_1*z03,l_2*z01-l_1*z02,l_3*z03*z33+z00*z03,l_2*z03*z33+z00*z02,l_1*z03*z33+z00*z01,l_3*z00*z33+l_0*z03*z33+z00^2,l_3^2*z33+l_3*z00-l_0*z03,l_2*l_3*z33+l_2*z00-l_0*z02,l_1*l_3*z33+l_1*z00-l_0*z01), ideal(z23,z22,z13,z12,z03,z02,z01,z00,l_1*z11+l_3*z33), ideal(z33,z23,z22,z13,z12,z11,l_3*z02-l_2*z03,l_3*z01-l_1*z03,l_2*z01-l_1*z02,l_3*z00-l_0*z03,l_2*z00-l_0*z02,l_1*z00-l_0*z01), ideal(z33,z23,z22,z13,z12,z03^2,z02*z03,z01*z03,z02^2,z01*z02,l_3*z02-l_2*z03,z01^2,l_3*z01-l_1*z03,l_2*z01-l_1*z02,l_1*z03*z11+z00*z03,l_1*z02*z11+z00*z02,l_1*z01*z11+z00*z01,l_1*z00*z11+l_0*z01*z11+z00^2,l_1*l_3*z11+l_3*z00-l_0*z03,l_1*l_2*z11+l_2*z00-l_0*z02,l_1^2*z11+l_1*z00-l_0*z01), ideal(z33,z23,z13,z12,z11,z03^2,z02*z03,z01*z03,z02^2,z01*z02,l_3*z02-l_2*z03,z01^2,l_3*z01-l_1*z03,l_2*z01-l_1*z02,l_2*z03*z22+z00*z03,l_2*z02*z22+z00*z02,l_1*z02*z22+z00*z01,l_2*z00*z22+l_0*z02*z22+z00^2,l_2*l_3*z22+l_3*z00-l_0*z03,l_2^2*z22+l_2*z00-l_0*z02,l_1*l_2*z22+l_1*z00-l_0*z01), ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_1*z11+l_2*z22)}
		{ideal(z23,z13,z12,z11,z03,z02,z01,z00,l_2*z22+l_3*z33), ideal(-z23,-z22,-z13,-z12,-z11,-z03,-z02,-z01,-l_3*z33-z00), ideal(z23,z22,z13,z12,z03,z02,z01,z00,l_1*z11+l_3*z33), ideal(z33,z23,z22,z13,z12,z11,l_3*z02-l_2*z03,l_3*z01-l_1*z03,l_2*z01-l_1*z02,l_3*z00-l_0*z03,l_2*z00-l_0*z02,l_1*z00-l_0*z01), ideal(-z33,-z23,-z22,-z13,-z12,-z03,-z02,-z01,-l_1*z11-z00), ideal(-z33,-z23,-z13,-z12,-z11,-z03,-z02,-z01,-l_2*z22-z00), ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_1*z11+l_2*z22)}

Inspecting the components of the primary decomposition and their radicals shows that for :math:`H(f) \cap X_2` consists of 4 reduced and 3 nonreduced points, completing the proof of Theorem 2.5.

In x-coordinates:

.. code-block:: macaulay2
		
		J=saturate(hessRank2(f,xCoords=>true),ideal(l_1*l_2*l_3));
		Psat = primaryDecomposition(J);
		print toString Psat
		print toString apply(Psat,e->radical e)

.. code-block:: macaulay2

		{ideal(x_3,x_2,x_1), ideal(x_3,x_0,x_1*l_1+x_2*l_2), ideal(x_1,x_0,x_2*l_2+x_3*l_3), ideal(x_3,x_1,x_0^2), ideal(x_2,x_1,x_0^2), ideal(x_2,x_0,x_1*l_1+x_3*l_3), ideal(x_3,x_2,x_0^2)}
		{ideal(x_1,x_2,x_3), ideal(x_3,x_0,x_1*l_1+x_2*l_2), ideal(x_1,x_0,x_2*l_2+x_3*l_3), ideal(x_0,x_1,x_3), ideal(x_0,x_1,x_2), ideal(x_2,x_0,x_1*l_1+x_3*l_3), ideal(x_0,x_2,x_3)}
		
Remark 2.7: special rank six cubics

.. code-block:: macaulay2

		f=l_0*x_0^3+x_1^3+x_2^3-3*x_0*(l_1*x_0*x_1+x_0*x_2+x_3^2);
		J=hessRank2(f);
		--We assume that all l_1 is nonzero:
		Jsat =saturate(J,ideal l_1);
		Psat = primaryDecomposition(Jsat);
		print toString Psat
		print toString apply(Psat,e->radical e)

.. code-block:: macaulay2

		{ideal(z23,z22,z13,z12,z02-z33,z33^2,z03*z33,z01*z33,l_1*z33-z01,z01*z11+z00*z33,l_1*z11+l_0*z33+z00,z03^2-z00*z33,z01*z03,z01^2), ideal(z23,z13,z12,z11,z02-z33,z33^2,z03*z33,z01*z33,z00*z33+z22*z33,l_1*z33-z01,l_0*z33+z00+z22,z03^2+z22*z33,z01*z03,z00*z03+z03*z22,z01^2,z00*z01+z01*z22,z00^2+2*z00*z22+z22^2,l_1*z00+l_0*z01+l_1*z22), ideal(z33,z23,z13,z12,z03,z02,z01,z00,l_1*z11+z22), ideal(z23,z22,z13,z12,z11,z02-z33,l_1*z33-z01,l_0*z33+z00,l_1*z00+l_0*z01,z33^3,z01*z33^2,z00*z33^2,z01^2*z33,z00^2*z33,z00^2*z01,z00^3)}
		{ideal(-z33,-z23,-z22,-z13,-z12,-z03,-z02,-z01,-l_1*z11-z00), ideal(z33,z23,z13,z12,z11,z03,z02,z01,z00+z22), ideal(-z33,-z23,-z13,-z12,-z03,-z02,-z01,-z00,-l_1*z11-z22), ideal(z33,z23,z22,z13,z12,z11,z02,z01,z00)}

In x-coordinates:

.. code-block:: macaulay2
		
		J=saturate(hessRank2(f,xCoords=>true),ideal l_1);
		Psat = primaryDecomposition(J);
		print toString Psat
		print toString apply(Psat,e->radical e)

.. code-block:: macaulay2

		{ideal(x_3,x_0,x_1*l_1+x_2), ideal(x_2,x_1,x_0^3), ideal(x_1,x_0*x_3,x_0*x_2-x_3^2,x_0^2), ideal(x_2,x_0*x_3,x_0^2,x_0*x_1*l_1-x_3^2)}
		{ideal(x_3,x_0,x_1*l_1+x_2), ideal (x_0,x_1,x_2), ideal(x_3,x_1,x_0), ideal(x_3,x_2,x_0)}

2.3 General singular cubics
===========================
It was already verified in 1.3 that they Cayley cubic does not lie on the Hurwitz form. We now do the same for a random singular cubic.

.. code-block:: macaulay2
		
		rand=flatten entries random(K^4,K^1);
		for i in 0..3 do(
		c_i=rand#i;
		);
		f=c_0*x_3*(x_1^2-x_0*x_2)+x_1*(x_0-(1+c_1)*x_1+c_1*x_2)*(x_0-(c_2+c_3)*x_1+c_2*c_3*x_2);
		isOnHessianDiscriminant(f)
		--false

The Cayley cubic again, in x-coordinates:

.. code-block:: macaulay2

		f=x_0*x_1*x_2+x_0*x_1*x_3+x_0*x_2*x_3+x_1*x_2*x_3;
		J=hessRank2(f,xCoords=>true);
		P = primaryDecomposition(J)

.. code-block:: macaulay2

		{ideal(x_3,x_1,x_0+x_2), ideal(x_3,x_2,x_0+x_1), ideal(x_3,x_1+x_2,x_0), ideal(x_2,x_1,x_0+x_3), ideal(x_2,x_1+x_3,x_0), ideal(x_2-x_3,x_1-x_3,x_0+x_3), ideal(x_2-x_3,x_1+x_3,x_0-x_3), ideal(x_2+x_3,x_1-x_3,x_0-x_3), ideal(x_2+x_3,x_1+x_3,x_0+x_3), ideal(x_2+x_3,x_1,x_0)}
