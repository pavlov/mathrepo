==============================================================
Likelihood degenerations
==============================================================

| This page contains auxiliary material to the paper: 
| Daniele Agostini, Taylor Brysiewicz, Claudia Fevola, Lukas Kühne, Bernd Sturmfels, and Simon Telen: Likelihood degenerations
| with an appendix by Thomas Lam
| ARXIV: https://arxiv.org/abs/2107.10518 CODE: https://mathrepo.mis.mpg.de/LikelihoodDegenerations/

Abstract:
Computing all critical points of a monomial
on a very affine variety is a fundamental task in algebraic
statistics, particle physics and other fields. The number of critical points is
known as the maximum likelihood (ML) degree. When the  variety 
is smooth, it coincides with the Euler characteristic.
We introduce degeneration techniques
that are inspired by the soft limits in CEGM theory, and we
answer several questions raised in the physics literature.
These pertain to bounded regions in discriminantal arrangements
and to moduli spaces of point configurations. 
We present theory and practise, connecting complex geometry, tropical combinatorics,
and numerical nonlinear algebra.

In the following we showcase two computations discussed in the paper in Jupyter notebooks.
The first presents the computation of ML degrees of matroid strata:

.. toctree::
  :maxdepth: 1
  :glob:

  ML_degrees_of_matroids

In the second notebook we compute the tropical solutions to the soft limit scattering equations as explained in Section 8 of the paper. 

.. toctree::
  :maxdepth: 1
  :glob:

  TropicalX37

These notebooks together with the underlying code and the matroid database can be downloaded :download:`here <LikelihoodDegenerationsSupplementaryMaterials.zip>`.
The files concerning the ML degrees of small matroids and their certification will be added soon.

Project page created: 22/07/2021

Project  contributors: Daniele Agostini, Taylor Brysiewicz, Claudia Fevola, Lukas Kühne, Bernd Sturmfels, and Simon Telen

Corresponding author of this page: Lukas Kühne, lukas.kuhne@mis.mpg.de

Software used: Julia (Version 1.6.0)
