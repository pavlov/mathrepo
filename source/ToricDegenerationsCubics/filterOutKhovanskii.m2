kk = QQ
R = kk[x1,x2,x3,x4,x5,x6,y1,y2,y3,y4,y5,y6]
E1 = x1
E2 = x2
E3 = x3
E4 = x4
E5 = x5
E6 = x6

load "monericClassesM2.txt";

-- degrees
DM = {
{0,1,0,0,0,0,0},
{0,0,1,0,0,0,0},
{0,0,0,1,0,0,0},
{0,0,0,0,1,0,0},
{0,0,0,0,0,1,0},
{0,0,0,0,0,0,1},
{1,0,0,1,1,1,1},
{1,0,1,0,1,1,1},
{1,0,1,1,0,1,1},
{1,0,1,1,1,0,1},
{1,0,1,1,1,1,0},
{1,1,0,0,1,1,1},
{1,1,0,1,0,1,1},
{1,1,0,1,1,0,1},
{1,1,0,1,1,1,0},
{1,1,1,0,0,1,1},
{1,1,1,0,1,0,1},
{1,1,1,0,1,1,0},
{1,1,1,1,0,0,1},
{1,1,1,1,0,1,0},
{1,1,1,1,1,0,0},
{2,2,1,1,1,1,1},
{2,1,2,1,1,1,1},
{2,1,1,2,1,1,1},
{2,1,1,1,2,1,1},
{2,1,1,1,1,2,1},
{2,1,1,1,1,1,2}
}

P = kk[e1,e2,e3,e4,e5,e6, f12,f13,f14,f15,f16,f23,f24,f25,f26,f34,f35,f36,f45,f46,f56, g1,g2,g3,g4,g5,g6,Degrees=>DM]

-- formula from [SturmfelsXu2010] for the multigraded Hilbert function
countPts = (lst)->(
  r = lst_0; u1 = lst_1; u2 = lst_2; u3 = lst_3; u4 = lst_4; u5 = lst_5; u6 = lst_6;
  np = 0;
  xl = max(0, 2*r-u2-u3-u4, 2*r-u2-u3-u6);
  xu = min(u1, u5, u1+u4+u5+u6-2*r);
  yl = max(0, 2*r-u1-u4-u6, 2*r-u4-u5-u6);
  yu = min(u2, u3, u1+u2+u3+u5-2*r);
  sl = max(r-u4, r-u6, 3*r-u2-u3-u4-u6);
  su = min(r, u1+u3+u5-r, u1+u2+u5-r);
        
  for i from xl to xu do (
    for j from yl to yu do (
      if ((i+j >= sl) and (i+j <= su) and (i-j >= r-u2-u3) and (2*i+j <= u1+u5) 
        and (i+2*j >= 2*r-u4-u6)) then (np = np + 1;);
    );
  );
  return(np);
);

-- number of all monomials in Ei, Fij, Gi in a given degree
-- ndm - number on DM list; only consider monomials with generators starting from that index
allMonDeg = (ndm, lst)->(
  s = 0;
  ldm = length DM;
  if (lst == {0,0,0,0,0,0,0}) then return 1;
  for i from ndm to ldm - 1 do (
    lpom = lst - DM_(ldm-1-i);
    nonneg = 1;
    for j from 0 to 6 do (if (lpom_j < 0) then nonneg = 0;);
    if (nonneg == 1) then (s = s + allMonDeg(i, lpom););
  );
  return s;
);

-- takes a list of monomials 
-- order: {E1,E2,E3,E4,E5,E6, F12,F13,F14,F15,F16,F23,F24,F25,F26,F34,F35,F36,F45,F46,F56, G1,G2,G3,G4,G5,G6}
-- and prints numbers of elements in graded pieces and the corresponding value of the Hilbert function
-- returns 1 if the input list is Khovanskii, 0 if not.
compareDeg = (lst) -> (
  rk = 1;
  M = map(R,P,matrix {lst});
  J = ker M;
  mj = mingens J;
  
  -- find all degrees
  LD = {};
  for i from 0 to numcols mj - 1 do (LD = append(LD,degree mj_i_0));
  LD = unique LD;

  -- divide relations into lists corresponding to degrees
  LG = {};
  for i from 0 to length LD - 1 do (
    pom = {};
    for j from 0 to numcols mj - 1 do (
      if (degree mj_j_0 == LD_i) then (pom = append(pom, mj_j_0););
    );
    LG = append(LG,pom);
  );

  -- print: index, degree, number of relations in this degree, value of the multigraded Hilbert function (Khovanskii <=> for all degrees the difference of the dim its graded piece minus the Hilbert function of the quotient ring is bigger than the number of relations between leading terms)
  for i from 0 to length LD - 1 do (
    cp = countPts(LD_i);
    am = allMonDeg(0, LD_i);
    --print(i, LD_i, length LG_i, cp, am-cp);
    if (am - cp - length LG_i < 0) then rk = 0;
  );
  return rk;
);

-- test for Khovanskii all the read in moneric classes
Khov = {};
resToricIdeals = {};
for i from 1 to length LL - 1 do
{
  rk = compareDeg(LL_i);
  print(i,rk);
  if rk == 1 then
  {
    Khov = append(Khov,LL_i);
    M = map(R,P,matrix {LL_i});
    J = ker M;
    resToricIdeals = append(resToricIdeals,J);
  };
};
"khovanskiiClasses.txt" << toString(Khov) << endl << close;
"toricDegenerations.txt" << toString(resToricIdeals) << endl << close;
