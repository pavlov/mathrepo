#####################
Certificate Gaussoids
#####################


The :download:`Certificate for valuated Gaussoids of rank 4 <CertificateGaussoids.sing>` contains a certificate which proves Theorem Conjecture 8.4 of

[BDKS18] T. Boege, A. D'Alì, T. Kahle and B. Sturmfels: *The Geometry of Gaussoids*, Found. Comput. Math. (2018). arXiv:1710.07175_

.. _arXiv:1710.07175: https://arxiv.org/abs/1710.07175


It works in the polynomial ring

.. math::
          \mathbb Q[p_{I}^{\pm 1}\mid I\subseteq [n]][a_{\{i,j\}|K}^{\pm 1}\mid i,j \in [n] \text{ distinct}, K\subseteq [n]\setminus\{i,j\}]

in which

* :math:`p_I` represents the principal minors of a symmetric :math:`n\times n` matrix
* :math:`a_{\{i,j\}|K}` represents the almost-principal minors of a symmetric :math:`n\times n` matrix.

The script contains a generating set :math:`F` for the ideal :math:`T_4` generated by the polynomial relations among these minors up to saturation as specified in Definition 4.1 of [BDKS18]. For a specific weight vector :math:`w_{\text{max}}\in\mathbb R^{\{p_I,a_{\{i,j\}|K}\}}`, the script shows

* :math:`w_{\text{max}}\in\text{Trop}(F)` by verifying that all initial forms :math:`\text{in}_{w_{\text{max}}}(f)` for :math:`f\in F` have at least 2 terms
* :math:`w_{\text{max}}\notin\text{Trop}(T_4)` by verifying that the initial ideal :math:`\text{in}_{w_{\text{max}}}(T_4)` contains the monomial :math:`a_{23}a_{23|1}`.


.. code-block:: none

                                    SINGULAR                              /  Development
                A Computer Algebra System for Polynomial Computations    /   version 4.1.1
                                                                       0<
                by: W. Decker, G.-M. Greuel, G. Pfister, H. Schoenemann  \   Feb 2018
                FB Mathematik der Universitaet, D-67653 Kaiserslautern    \
                > LIB "tropicalBasis.lib"; // initializes necessary libraries and helper functions
                > intvec wMin = 14,10,6,0,6,8,8,2,8,6,6,2,8,8,8,8,8,4,2,10,9,3,5,5,9,11,
                .   1,5,7,5,5,5,7,7,1,5,8,6,4,4; // wMin is in min-convention
                > intvec wMax = -wMin;           // Singular uses max-convention
                > intvec allOnes = onesVector(size(wMax));
                > ring r = 0,(p,p1,p12,p123,p1234,p124,p13,p134,p14,p2,p23,p234,p24,p3,p34,p4,
                .   a12,a12_3,a12_34,a12_4,a13,a13_2,a13_24,a13_4,a14,a14_2,a14_23,a14_3,
                .   a23,a23_1,a23_14,a23_4,a24,a24_1,a24_13,a24_3,a34,a34_1,a34_12,a34_2),
                .   (a(allOnes),a(wMax),lp); // prepending allOnes makes no difference mathematically
                                             // as the ideal is homogeneous,
                                             // but it helps computationally
                > ideal F =                  // Singular ideals are list of polynomials
                .   a34_12*a13_24+p124*a14_23-a14_2*p1234,
                    [...]                    // see uploaded script
                .   -p1*p2+a12^2+p*p12;
                > ideal inF = initial(F,wMax); // initial forms of the elements in F
                                               // all are at least binomial, hence wMas lies in Trop(F)
                > ideal I = groebner(F);
                > ideal inI = initial(I,wMax); // initial forms of all elements in the Gr\"obner basis
                                               // this is a Groebner basis of in_wMax(I)
                > NF(a23*a23_1,inI);           // normal form is 0 hence a23*a23_1 lies in in_wMax(I)
                0
