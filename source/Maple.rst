####################
 Maple
####################

- :doc:`DAlgebraicFunctions/index`

- :doc:`InvitationToNonlinearAlgebra/index`

- :doc:`KPSolitonsFromTropicalLimits/index`
  
.. toctree::
   :maxdepth: 0
