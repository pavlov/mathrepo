==============================================================
Visualizing the image of the entropy map when :math:`d = 3`.
==============================================================
gi

.. code-block:: polymake

		## The inequalities defining the bipyramid Ccal

		$E =[[ 1,   1,   1,   1],
		     [ 1,   0,   1,   2],
		     [-1,  -1,  -1,  -2],
		     [ 0,  -1,   1,   0],
		     [ 0,   0,  -1,   0],
		     [ 0,   1,  -1,  -1]];


		## The 6 inequalities defining the facets of the bipyramid Ccal
		# Eij means the facet of "i independent from j"


		$E12 =[[ 1,   1,   1,   1],
			   [-1,  -1,  -1,  -1],
		       [ 1,   0,   1,   2],
		       [-1,  -1,  -1,  -2],
		       [ 0,  -1,   1,   0], 
		       [ 0,   0,  -1,   0], 
		       [ 0,   1,  -1,  -1]];



		$E13 =[[ 1,   1,   1,   1],
		       [ 1,   0,   1,   2],       
		       [-1,  -1,  -1,  -2],       
		       [ 0,  -1,   1,   0],       
		       [ 0,   1,  -1,   0],       
		       [ 0,   0,  -1,   0],     
		       [ 0,   1,  -1,  -1]];



		$E23 =[[ 1,   1,   1,   1],
		       [ 1,   0,   1,   2],
		       [-1,   0,  -1,  -2],
		       [-1,  -1,  -1,  -2],
		       [ 0,  -1,   1,   0],
		       [ 0,   0,  -1,   0], 
		       [ 0,   1,  -1,  -1]];


		# Eij_k means the facet of "i independent from j given k"

		$E12_3 =[[ 1,   1,   1,   1], 
				 [ 1,   0,   1,   2], 
				 [-1,  -1,  -1,  -2],         
				 [ 0,  -1,   1,   0],         
				 [ 0,   0,  -1,   0],         
				 [ 0,   1,  -1,  -1], 
				 [ 0,  -1,   1,   1]];




		$E13_2 =[[1, 1, 1, 1], 
				 [1, 0, 1, 2], 
				 [-1,-1,-1,-2], 
				 [1, 1, 1, 2], 
				 [0, -1, 1, 0], 
				 [0, 0, -1, 0] , 
				 [0, 1, -1, -1]];




		$E23_1 =[[ 1,   1,   1,   1], 
				 [ 1,   0,   1,   2], 
				 [-1,  -1,  -1,  -2], 
				 [ 0,  -1,   1,  0], 
				 [ 0,   0,  -1,   0], 
				 [ 0,   0,   1,   0], 
				 [ 0,   1,  -1,  -1]];




		#The three polyhedera that make the image of H in dimension 3.


		$C1 = [[1,1,1,1],[-1,-2,0,-1],[0,0,-1,0],[0,1,0,-1],[0,-1,0,1]];
		$C2 = [[1,1,1,1],[0,-1,1,0],[0,0,-1,0],[1,2,0,1],[1,1,0,2],[-1,-1,0,-2]];
		$C3 = [[1,1,1,1],[0,0,-1,0],[-1,-1,-1,-2],[0,-1,0,1],[1,2,0,1],[-1,-2,0,-1]];




		$P = new Polytope(INEQUALITIES=>$E); $P12   = new Polytope(INEQUALITIES=>$E12); $P13   = new Polytope(INEQUALITIES=>$E13); $P23   = new Polytope(INEQUALITIES=>$E23); $P12_3 = new Polytope(INEQUALITIES=>$E12_3); $P13_2 = new Polytope(INEQUALITIES=>$E13_2); $P23_1 = new Polytope(INEQUALITIES=>$E23_1);


		$Ccal1 = new Polytope(INEQUALITIES=>$C1);  $Ccal2 = new Polytope(INEQUALITIES=>$C2);  $Ccal3 = new Polytope(INEQUALITIES=>$C3); 




		compose($P->VISUAL(FacetTransparency=>0.2), $P12->VISUAL FacetColor=>"red",FacetTransparency=>0.7), $P13->VISUAL(FacetColor=>"blue",FacetTransparency=>0.7), $P23->VISUAL(FacetColor=>"green",FacetTransparency=>0.7), $P12_3->VISUAL(FacetColor=>"orange"), $P13_2->VISUAL(FacetColor=>"yellow"),$P23_1->VISUAL(FacetColor=>"black"),);


		compose($P->VISUAL(FacetTransparency=>0.2), $P12->VISUAL(FacetColor=>"red", FacetTransparency=>0.7), $P13->VISUAL(FacetColor=>"blue",FacetTransparency=>0.7), $P23->VISUAL(FacetColor=>"green", FacetTransparency=>0.7), $P12_3->VISUAL(FacetColor=>"orange"), $P13_2->VISUAL(FacetColor=>"yellow"),$P23_1->VISUAL(FacetColor=>"black"), $Ccal1->VISUAL,$Ccal2->VISUAL,$Ccal3->VISUAL);


		compose($P->VISUAL(FacetTransparency=>0.2),$Ccal1->VISUAL(FacetColor=>"green"),$Ccal2->VISUAL(FacetColor=>"green"),$Ccal3->VISUAL(FacetColor=>"green"));